import React from 'react';
import moment from 'moment';

class Clock extends React.Component {
  constructor(props) {
    super(props);
    this.state = {date: moment().format('MMMM Do YYYY, h:mm:ss a')};
  }

  componentDidMount() {
    this.interval = setInterval(() => this.second(), 1000);
  }

  second() {
    this.setState({
      date: moment().format('MMMM Do YYYY, h:mm:ss a')
    });
  }

  render() {
    return (
      <div>
        <h1>New York</h1>
        {this.state.date}    
      </div>
    );
  }
}

export default Clock